/********************************************************************************
** Form generated from reading UI file 'RegisterWin.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_REGISTERWIN_H
#define UI_REGISTERWIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_RegisterWin
{
public:
    QHBoxLayout *horizontalLayout_8;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *nameEdit;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QRadioButton *mrb;
    QRadioButton *wrb;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_3;
    QDateEdit *birthdayEdit;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_4;
    QLineEdit *addressEdit;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_5;
    QLineEdit *phoneEdit;
    QHBoxLayout *horizontalLayout_6;
    QPushButton *registerBt;
    QPushButton *resetBt;
    QVBoxLayout *verticalLayout_2;
    QLabel *headpicLb;
    QLineEdit *headfile;
    QHBoxLayout *horizontalLayout_7;
    QPushButton *addheadBt;
    QPushButton *openvideoBt;
    QPushButton *cameraBt;

    void setupUi(QWidget *RegisterWin)
    {
        if (RegisterWin->objectName().isEmpty())
            RegisterWin->setObjectName(QString::fromUtf8("RegisterWin"));
        RegisterWin->resize(819, 607);
        horizontalLayout_8 = new QHBoxLayout(RegisterWin);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        horizontalLayout_8->setContentsMargins(0, 0, 0, 0);
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(RegisterWin);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy);
        QFont font;
        font.setPointSize(15);
        label->setFont(font);

        horizontalLayout->addWidget(label);

        nameEdit = new QLineEdit(RegisterWin);
        nameEdit->setObjectName(QString::fromUtf8("nameEdit"));
        nameEdit->setEnabled(true);
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(nameEdit->sizePolicy().hasHeightForWidth());
        nameEdit->setSizePolicy(sizePolicy1);

        horizontalLayout->addWidget(nameEdit);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(RegisterWin);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        sizePolicy.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy);
        label_2->setFont(font);

        horizontalLayout_2->addWidget(label_2);

        mrb = new QRadioButton(RegisterWin);
        mrb->setObjectName(QString::fromUtf8("mrb"));

        horizontalLayout_2->addWidget(mrb);

        wrb = new QRadioButton(RegisterWin);
        wrb->setObjectName(QString::fromUtf8("wrb"));

        horizontalLayout_2->addWidget(wrb);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_3 = new QLabel(RegisterWin);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        sizePolicy.setHeightForWidth(label_3->sizePolicy().hasHeightForWidth());
        label_3->setSizePolicy(sizePolicy);
        label_3->setFont(font);

        horizontalLayout_3->addWidget(label_3);

        birthdayEdit = new QDateEdit(RegisterWin);
        birthdayEdit->setObjectName(QString::fromUtf8("birthdayEdit"));
        sizePolicy1.setHeightForWidth(birthdayEdit->sizePolicy().hasHeightForWidth());
        birthdayEdit->setSizePolicy(sizePolicy1);
        birthdayEdit->setFont(font);
        birthdayEdit->setFocusPolicy(Qt::NoFocus);
        birthdayEdit->setContextMenuPolicy(Qt::NoContextMenu);
        birthdayEdit->setWrapping(false);
        birthdayEdit->setFrame(true);
        birthdayEdit->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        birthdayEdit->setButtonSymbols(QAbstractSpinBox::UpDownArrows);
        birthdayEdit->setProperty("showGroupSeparator", QVariant(false));

        horizontalLayout_3->addWidget(birthdayEdit);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_4 = new QLabel(RegisterWin);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        sizePolicy.setHeightForWidth(label_4->sizePolicy().hasHeightForWidth());
        label_4->setSizePolicy(sizePolicy);
        label_4->setFont(font);

        horizontalLayout_4->addWidget(label_4);

        addressEdit = new QLineEdit(RegisterWin);
        addressEdit->setObjectName(QString::fromUtf8("addressEdit"));
        sizePolicy1.setHeightForWidth(addressEdit->sizePolicy().hasHeightForWidth());
        addressEdit->setSizePolicy(sizePolicy1);

        horizontalLayout_4->addWidget(addressEdit);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_5 = new QLabel(RegisterWin);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        sizePolicy.setHeightForWidth(label_5->sizePolicy().hasHeightForWidth());
        label_5->setSizePolicy(sizePolicy);
        label_5->setFont(font);

        horizontalLayout_5->addWidget(label_5);

        phoneEdit = new QLineEdit(RegisterWin);
        phoneEdit->setObjectName(QString::fromUtf8("phoneEdit"));
        sizePolicy1.setHeightForWidth(phoneEdit->sizePolicy().hasHeightForWidth());
        phoneEdit->setSizePolicy(sizePolicy1);

        horizontalLayout_5->addWidget(phoneEdit);


        verticalLayout->addLayout(horizontalLayout_5);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        registerBt = new QPushButton(RegisterWin);
        registerBt->setObjectName(QString::fromUtf8("registerBt"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(registerBt->sizePolicy().hasHeightForWidth());
        registerBt->setSizePolicy(sizePolicy2);
        registerBt->setFont(font);

        horizontalLayout_6->addWidget(registerBt);

        resetBt = new QPushButton(RegisterWin);
        resetBt->setObjectName(QString::fromUtf8("resetBt"));
        sizePolicy2.setHeightForWidth(resetBt->sizePolicy().hasHeightForWidth());
        resetBt->setSizePolicy(sizePolicy2);
        resetBt->setFont(font);

        horizontalLayout_6->addWidget(resetBt);


        verticalLayout->addLayout(horizontalLayout_6);


        horizontalLayout_8->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        headpicLb = new QLabel(RegisterWin);
        headpicLb->setObjectName(QString::fromUtf8("headpicLb"));
        headpicLb->setStyleSheet(QString::fromUtf8("background-color: rgb(114, 159, 207);"));

        verticalLayout_2->addWidget(headpicLb);

        headfile = new QLineEdit(RegisterWin);
        headfile->setObjectName(QString::fromUtf8("headfile"));

        verticalLayout_2->addWidget(headfile);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        addheadBt = new QPushButton(RegisterWin);
        addheadBt->setObjectName(QString::fromUtf8("addheadBt"));
        sizePolicy2.setHeightForWidth(addheadBt->sizePolicy().hasHeightForWidth());
        addheadBt->setSizePolicy(sizePolicy2);
        addheadBt->setFont(font);

        horizontalLayout_7->addWidget(addheadBt);

        openvideoBt = new QPushButton(RegisterWin);
        openvideoBt->setObjectName(QString::fromUtf8("openvideoBt"));
        sizePolicy2.setHeightForWidth(openvideoBt->sizePolicy().hasHeightForWidth());
        openvideoBt->setSizePolicy(sizePolicy2);
        openvideoBt->setFont(font);

        horizontalLayout_7->addWidget(openvideoBt);

        cameraBt = new QPushButton(RegisterWin);
        cameraBt->setObjectName(QString::fromUtf8("cameraBt"));
        sizePolicy2.setHeightForWidth(cameraBt->sizePolicy().hasHeightForWidth());
        cameraBt->setSizePolicy(sizePolicy2);
        cameraBt->setFont(font);

        horizontalLayout_7->addWidget(cameraBt);


        verticalLayout_2->addLayout(horizontalLayout_7);

        verticalLayout_2->setStretch(0, 3);
        verticalLayout_2->setStretch(1, 1);
        verticalLayout_2->setStretch(2, 1);

        horizontalLayout_8->addLayout(verticalLayout_2);


        retranslateUi(RegisterWin);

        QMetaObject::connectSlotsByName(RegisterWin);
    } // setupUi

    void retranslateUi(QWidget *RegisterWin)
    {
        RegisterWin->setWindowTitle(QCoreApplication::translate("RegisterWin", "Form", nullptr));
        label->setText(QCoreApplication::translate("RegisterWin", "\345\247\223\345\220\215", nullptr));
        label_2->setText(QCoreApplication::translate("RegisterWin", "\346\200\247\345\210\253", nullptr));
        mrb->setText(QCoreApplication::translate("RegisterWin", "\347\224\267", nullptr));
        wrb->setText(QCoreApplication::translate("RegisterWin", "\345\245\263", nullptr));
        label_3->setText(QCoreApplication::translate("RegisterWin", "\347\224\237\346\227\245", nullptr));
        label_4->setText(QCoreApplication::translate("RegisterWin", "\345\234\260\345\235\200", nullptr));
        label_5->setText(QCoreApplication::translate("RegisterWin", "\347\224\265\350\257\235", nullptr));
        registerBt->setText(QCoreApplication::translate("RegisterWin", "\346\263\250\345\206\214", nullptr));
        resetBt->setText(QCoreApplication::translate("RegisterWin", "\351\207\215\347\275\256", nullptr));
        headpicLb->setText(QString());
        addheadBt->setText(QCoreApplication::translate("RegisterWin", "\346\267\273\345\212\240\345\244\264\345\203\217", nullptr));
        openvideoBt->setText(QCoreApplication::translate("RegisterWin", "\346\211\223\345\274\200\346\221\204\345\203\217\345\244\264", nullptr));
        cameraBt->setText(QCoreApplication::translate("RegisterWin", "\346\213\215\346\221\204", nullptr));
    } // retranslateUi

};

namespace Ui {
    class RegisterWin: public Ui_RegisterWin {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_REGISTERWIN_H
