/********************************************************************************
** Form generated from reading UI file 'face_server.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FACE_SERVER_H
#define UI_FACE_SERVER_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QWidget>
#include <RegisterWin.h>
#include <selectWin.h>

QT_BEGIN_NAMESPACE

class Ui_face_server
{
public:
    QWidget *centralwidget;
    QGridLayout *gridLayout;
    QTabWidget *tabWidget;
    QWidget *attWg;
    QLabel *headLb;
    RegisterWin *regWg;
    selectWin *seleWg;

    void setupUi(QMainWindow *face_server)
    {
        if (face_server->objectName().isEmpty())
            face_server->setObjectName(QString::fromUtf8("face_server"));
        face_server->resize(800, 600);
        centralwidget = new QWidget(face_server);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        gridLayout = new QGridLayout(centralwidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        tabWidget = new QTabWidget(centralwidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        attWg = new QWidget();
        attWg->setObjectName(QString::fromUtf8("attWg"));
        headLb = new QLabel(attWg);
        headLb->setObjectName(QString::fromUtf8("headLb"));
        headLb->setGeometry(QRect(-70, -10, 400, 400));
        headLb->setStyleSheet(QString::fromUtf8("background-color: rgb(138, 226, 52);"));
        tabWidget->addTab(attWg, QString());
        regWg = new RegisterWin();
        regWg->setObjectName(QString::fromUtf8("regWg"));
        regWg->setContextMenuPolicy(Qt::DefaultContextMenu);
        tabWidget->addTab(regWg, QString());
        seleWg = new selectWin();
        seleWg->setObjectName(QString::fromUtf8("seleWg"));
        tabWidget->addTab(seleWg, QString());

        gridLayout->addWidget(tabWidget, 0, 0, 1, 1);

        face_server->setCentralWidget(centralwidget);

        retranslateUi(face_server);

        tabWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(face_server);
    } // setupUi

    void retranslateUi(QMainWindow *face_server)
    {
        face_server->setWindowTitle(QCoreApplication::translate("face_server", "face_server", nullptr));
        headLb->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(attWg), QCoreApplication::translate("face_server", "\350\200\203\345\213\244\345\233\276\345\203\217", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(regWg), QCoreApplication::translate("face_server", "\346\263\250\345\206\214", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(seleWg), QCoreApplication::translate("face_server", "\346\237\245\350\257\242", nullptr));
    } // retranslateUi

};

namespace Ui {
    class face_server: public Ui_face_server {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FACE_SERVER_H
