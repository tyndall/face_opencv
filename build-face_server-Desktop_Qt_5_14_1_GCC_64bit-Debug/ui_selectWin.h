/********************************************************************************
** Form generated from reading UI file 'selectWin.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SELECTWIN_H
#define UI_SELECTWIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_selectWin
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QRadioButton *empRb;
    QRadioButton *attRb;
    QPushButton *quertBt;
    QTableView *tableView;

    void setupUi(QWidget *selectWin)
    {
        if (selectWin->objectName().isEmpty())
            selectWin->setObjectName(QString::fromUtf8("selectWin"));
        selectWin->resize(653, 486);
        verticalLayout = new QVBoxLayout(selectWin);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        empRb = new QRadioButton(selectWin);
        empRb->setObjectName(QString::fromUtf8("empRb"));

        horizontalLayout->addWidget(empRb);

        attRb = new QRadioButton(selectWin);
        attRb->setObjectName(QString::fromUtf8("attRb"));

        horizontalLayout->addWidget(attRb);

        quertBt = new QPushButton(selectWin);
        quertBt->setObjectName(QString::fromUtf8("quertBt"));

        horizontalLayout->addWidget(quertBt);


        verticalLayout->addLayout(horizontalLayout);

        tableView = new QTableView(selectWin);
        tableView->setObjectName(QString::fromUtf8("tableView"));

        verticalLayout->addWidget(tableView);


        retranslateUi(selectWin);

        QMetaObject::connectSlotsByName(selectWin);
    } // setupUi

    void retranslateUi(QWidget *selectWin)
    {
        selectWin->setWindowTitle(QCoreApplication::translate("selectWin", "Form", nullptr));
        empRb->setText(QCoreApplication::translate("selectWin", "employee", nullptr));
        attRb->setText(QCoreApplication::translate("selectWin", "attendance", nullptr));
        quertBt->setText(QCoreApplication::translate("selectWin", "\346\237\245\350\257\242", nullptr));
    } // retranslateUi

};

namespace Ui {
    class selectWin: public Ui_selectWin {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SELECTWIN_H
