#ifndef FACE_SERVER_H
#define FACE_SERVER_H

#include <QMainWindow>
#include <QtNetwork>
#include<QTcpServer>
#include<QTcpSocket>
#include<opencv.hpp>
#include"qfaceobject.h"
#include<QThread>
#include<QSqlDatabase>
#include<QSqlTableModel>
#include<QSqlRecord>
QT_BEGIN_NAMESPACE
namespace Ui { class face_server; }
QT_END_NAMESPACE

class face_server : public QMainWindow
{
    Q_OBJECT

public:
    face_server(QWidget *parent = nullptr);
    ~face_server();
signals:
    void query(cv::Mat &faceImage);

private:
    Ui::face_server *ui;

    //a声明服务器对象和代替服务器收发数据的对象（tcpserver只能链接不能收发数据）
    QTcpServer mserver;
    QTcpSocket *msocket;
    //读取客户端stream传来的字节数
    qint64 bsize;
    QFaceObject face_object;
    QSqlTableModel model;

protected slots:
    void accept_client();
    void read_data();
    void recv_faceID(int64_t faceID);
};
#endif // FACE_SERVER_H
