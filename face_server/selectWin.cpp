#include "selectWin.h"
#include "ui_selectWin.h"
#include<QButtonGroup>
selectWin::selectWin(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::selectWin)
{
    ui->setupUi(this);
    model=new QSqlTableModel();
    //用于两个按钮互斥
    QButtonGroup* buttonGroup = new QButtonGroup(this);
    buttonGroup->addButton(ui->attRb);
    buttonGroup->addButton(ui->empRb);
    buttonGroup->setExclusive(true);
}

selectWin::~selectWin()
{
    delete ui;
}

void selectWin::on_quertBt_clicked()
{
    if(ui->empRb->isChecked()){
        model->setTable("employee");
    }
    if(ui->attRb->isChecked()){
        model->setTable("attendance");
    }
    model->select();
    ui->tableView->setModel(model);
}
