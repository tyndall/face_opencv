#include "RegisterWin.h"
#include "ui_RegisterWin.h"
#include<QFileDialog>
#include"qfaceobject.h"
#include<QSqlDatabase>
#include<QSqlTableModel>
#include<QSqlRecord>
#include<QMessageBox>
#include<QString>
#include<QDebug>
RegisterWin::RegisterWin(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RegisterWin)
{
    ui->setupUi(this);
}

RegisterWin::~RegisterWin()
{
delete ui;
}
//重置页面数据
void RegisterWin::on_resetBt_clicked()
{
    ui->nameEdit->clear();
    ui->birthdayEdit->setDate(QDate::currentDate());
    ui->addressEdit->clear();
    ui->headfile->clear();
    ui->headpicLb->clear();
}
//添加头像
void RegisterWin::on_addheadBt_clicked()
{
    QString path=QFileDialog::getOpenFileName(this);
    ui->headfile->setText(path);

    QPixmap mmp(path);
    mmp.scaledToWidth(ui->headpicLb->width());
    ui->headpicLb->setPixmap(mmp);
    //ui->headpicLb->setAlignment(Qt::AlignCenter);

}
//注册

void RegisterWin::on_registerBt_clicked()
{
    //1通过照片结合faceobjectt类得到faceid
    QFaceObject faceobject;
    cv::Mat image = cv::imread(ui->headfile->text().toUtf8().data());
    if(image.data==nullptr){
        qDebug()<<"imread失败";
    }
    else{
            qDebug()<<"imread成功";
    }
    int faceID=faceobject.face_register(image);
    qDebug()<<"face_id"<<faceID;
    //2设置一个存储employee信息的数据库表
    QSqlTableModel model;
    model.setTable("employee");
    model.select();
    qDebug()<<"register 的rowcount"<<model.rowCount()<<model.select();

    //3头像保存到一个路径(一定要在Debug中去创建一个data文件，否则保存不了)
    QString headfile=QString("./data/%1.jpg").arg(QString(ui->nameEdit->text().toUtf8().toBase64()));
    if(cv::imwrite(headfile.toUtf8().data(),image)){
        qDebug()<<"读取图片保存成功 读取的路径为:"<<headfile.toUtf8().data()<<" "<<headfile;
    }
    else{
        qDebug()<<"读取图片保存失败 读取的路径为: "<<headfile.toUtf8().data()<<" "<<headfile;
    }
    //4.设置record记录的值
    QSqlRecord record=model.record();
    record.setValue("name",ui->nameEdit->text());
    record.setValue("sex",ui->mrb->isChecked()?"男":"女");
    record.setValue("birthday",ui->birthdayEdit->text());
    record.setValue("address",ui->addressEdit->text());
    record.setValue("phone",ui->phoneEdit->text());
    record.setValue("faceID",faceID);
    record.setValue("headfile",headfile);
    //5.插入记录到模型，插入成功则提交
    bool ret=model.insertRecord(0,record);
    if(ret){
        model.submitAll();
        QMessageBox::information(nullptr,"信息","注册成功");
    }
    else{
        QMessageBox::information(nullptr,"信息","注册失败,头像错误或者信息错误");
    }


}

void RegisterWin::timerEvent(QTimerEvent *e)
{
    //cv::Mat srcImage;//1 用于接收摄像头数据(opencv格式，摄像头也是opencv实例化的)
    if(cap.isOpened()){
        cap>>srcImage;//1.1读取摄像头数据到srcimage中
        if(srcImage.data==nullptr){
            return ;
        }
    }
    //2转为qt需要的图像格式 qpixmap需要输入为qimage格式 所以先转为qimage格式 并且 qt的颜色是rgb 不是cv的bgr 也要转
    cv::Mat rgbImage;
    cv::cvtColor(srcImage,rgbImage,cv::COLOR_BGR2RGB);
    //cv::flip(outImage,outImage,1);//有些摄像头可能是镜像的 所以要翻转


    QImage qimage(rgbImage.data,rgbImage.cols,rgbImage.rows,rgbImage.step1(),QImage::Format_RGB888);
    QPixmap mmp=QPixmap::fromImage(qimage);

    //3qt上显示图像
    mmp=mmp.scaledToWidth(ui->headpicLb->width());
    ui->headpicLb->setPixmap(mmp);
}

void RegisterWin::on_openvideoBt_clicked()
{
    if(ui->openvideoBt->text()=="打开摄像头"){
        if(cap.open(0)){
            ui->openvideoBt->setText("关闭摄像头");
            timerID=startTimer(100);
        }
    }
    else{
        killTimer(timerID);
        ui->openvideoBt->setText("打开摄像头");
        //关闭指定计时器

        //释放摄像头资源
        cap.release();
    }
}

void RegisterWin::on_cameraBt_clicked()
{
    //保存拍摄的数据
    QString hfile=QString("./data/%1.jpg").arg(QString(ui->nameEdit->text().toUtf8().toBase64()));//使用Base64编码可以确保文件路径只包含标准ASCII字符，而不会包含可能引起问题的特殊字符如"/"。
    qDebug()<<"拍摄"<<hfile;
    ui->headfile->setText(hfile);
    if(cv::imwrite(hfile.toUtf8().data(),srcImage)){
        qDebug()<<"保存成功，路径为"<<hfile.toUtf8().data();
    }
    else{
        qDebug()<<"保存失败，路径为"<<hfile.toUtf8().data();
    }
    killTimer(timerID);
    ui->openvideoBt->setText("打开摄像头");
    cap.release();
    QMessageBox::information(nullptr,"信息","拍照成功");



}
