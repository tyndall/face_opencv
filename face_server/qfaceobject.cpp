#include "qfaceobject.h"
#include<QDebug>
QFaceObject::QFaceObject(QObject *parent) : QObject(parent)
{
    // 1、初始化 seeta 人脸引擎设置
    seeta::ModelSetting FDmodel("../model/fd_2_00.dat",seeta::ModelSetting::CPU,0);
    seeta::ModelSetting PDmodel("../model/pd_2_00_pts5.dat",seeta::ModelSetting::CPU,0);
    seeta::ModelSetting FRmodel("../model/fr_2_10.dat",seeta::ModelSetting::CPU,0);
    // 2、创建 seeta 人脸引擎对象
    this->fengineptr=new seeta::FaceEngine(FDmodel,PDmodel,FRmodel);
    //要先运行一遍后 保存了人脸数据库后 再加载人脸数据库 也就是第一次使用 不要这行代码  因为没有face.db 运行后了才有
    this->fengineptr->Load("./face.db");

}

QFaceObject::~QFaceObject(){
    delete fengineptr;
}
//3、注册人脸信息并且保存到数据库
int64_t QFaceObject::face_register(cv::Mat &faceImage)
{
    //3.1、把opencv的图像歌声转为seeta的图像格式
   SeetaImageData simage;
   simage.data=faceImage.data;
   simage.width=faceImage.cols;
   simage.height=faceImage.rows;
   simage.channels=faceImage.channels();
    //3.2、注册人脸返回id
   int faceid=this->fengineptr->Register(simage);

   if(faceid>=0){
       //就是将当前 SeetaFace 引擎内部的人脸数据库保存到当前目录下的 face.db 文件中
       this->fengineptr->Save("./face.db");
   }
   else{
       qDebug()<<"Register false";
   }
   return faceid;

}
//4、查询人脸信息
int64_t QFaceObject::face_query(cv::Mat &faceImage)
{
    //4.1把opencv的图像歌声转为seeta的图像格式
   SeetaImageData simage;
   simage.data=faceImage.data;
   simage.width=faceImage.cols;
   simage.height=faceImage.rows;
   simage.channels=faceImage.channels();
   //4.2声明相似度变量
   float similarity=0;
   //4.3调用人脸引擎进行人脸识别
   int64_t faceid=this->fengineptr->Query(simage,&similarity);

   if(similarity>=0.7){
        //4.4发送信号用于服务器接受faceid
        qDebug()<<"查询到的faceid和匹配度为 "<<faceid<<"   "<<similarity;
        emit send_faceID(faceid);
   }
   else{
       emit send_faceID(-1);
   }
   return faceid;
}
