#include "face_server.h"
#include"RegisterWin.h"
#include "selectWin.h"
#include <QSqlDatabase>
#include<QSqlQuery>
#include<QSqlError>
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    qRegisterMetaType<cv::Mat>("cv::Mat&");
    qRegisterMetaType<cv::Mat>("cv::Mat");
    qRegisterMetaType<int64_t>("int64_t");


    //1.连接数据库
    QSqlDatabase db=QSqlDatabase::addDatabase("QSQLITE");
    //2.设置数据库的名称
    db.setDatabaseName("server.db");
    //3、检测数据库链接是否成功
    if(!db.open()){
        qDebug()<<db.lastError().text();//打印最后的错误信息

        return -1;
    }
    //4.1创建员工信息表
    QString createsql="create table if not exists employee(employeeID integer primary key autoincrement,"
                      "name varchar(256),sex varchar(32),birthday text,address text,phone text,faceId integer unique,headfile ,text)";
    //4.1.1创建sql查询对象
    QSqlQuery query;
    if(!query.exec(createsql)){
        qDebug()<<query.lastError().text();
        return -1;
    }
//4.2创建考勤信息表
    createsql="create table if not exists attendance(attendanceID integer primary key autoincrement,employeeID integer,"
                "attendanceTime TimeStamp NOT NULL DEFAULT(datetime('now','localtime')))";

    if(!query.exec(createsql)){
        qDebug()<<query.lastError().text();

        return -1;
    }
    face_server w;
    //RegisterWin m;
   // selectWin   s;
   // m.show();
    w.show();
   // s.show();
    return a.exec();
}
