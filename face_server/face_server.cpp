#include "face_server.h"
#include "ui_face_server.h"
#include<QSqlError>
#include <QDateTime>
#include <QSqlRecord>
#include <QThread>
#include <opencv.hpp>
#include <QSqlQuery>
#include <QSqlError>

face_server::face_server(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::face_server)
{

    ui->setupUi(this);

    //2、监听到客户端会产生newconnection信号，然后建立客户端链接
    connect(&mserver,&QTcpServer::newConnection,this,&face_server::accept_client);
    //1、服务器开始监听
    mserver.listen(QHostAddress::Any,7776);
    bsize=0;

    //给sql模型绑定表格
    model.setTable("employee");

    //通过线程执行查询
    QThread *pthread=new QThread();
    face_object.moveToThread(pthread);
    pthread->start();
    //开始识别人脸的时候调用线程（相当于不在本gui线程处理人脸识别，去其他线程处理）
    connect(this,&face_server::query,&face_object,&QFaceObject::face_query);
    connect(&face_object,&QFaceObject::send_faceID,this,&face_server::recv_faceID);


}

face_server::~face_server()
{
    delete ui;
}
//2.1 建立链接 用tcpsocket代替服务器随时关注客户端有没有发数据
void face_server::accept_client()
{
    qDebug()<<"链接客户端成功";
    msocket=mserver.nextPendingConnection();
    connect(msocket,&QTcpSocket::readyRead,this,&face_server::read_data);
}
//2.2读取服务器（msocket代替接收的）收到的数据
void face_server::read_data()
{
    qDebug()<<"ready read";
    // a1创建 QDataStream 对象，并设置其版本为 Qt_5_14
    QDataStream stream(msocket);
    stream.setVersion(QDataStream::Qt_5_14);

    // 如果 bsize 为零，则检查套接字中是否有足够的数据读取数据大小
    if (bsize == 0) {
        if (msocket->bytesAvailable() < (qint64)sizeof(bsize)) {
            // 数据不足，返回
            return;
        }
        //a2 读取数据大小并将其赋予 bsize
        stream >> bsize;
        qDebug()<<"读取到的bsize为："<<bsize;
    }

    // 检查套接字中是否有足够的数据读取图像数据
    if (msocket->bytesAvailable() < bsize) {
        // 数据不足，返回
        qDebug()<<"数据不足1";
        return;
    }

    // a3读取图像数据并将其赋予 data
    QByteArray data;
    stream >> data;

    // a4将 bsize 重置为零
    bsize = 0;

    // 如果 data 为空，则返回
    if (data.size() == 0) {
        return;
    }
    // a5使用 loadFromData() 函数从 data 中加载图像
    QPixmap m_mp;
    if (!m_mp.loadFromData(data, "jpg")) {
        // 加载图像失败，可能是数据损坏或格式错误
        return;
    }

    // a6将图像缩放到 QLabel 控件的大小 显示在 QLabel 控件上
    m_mp = m_mp.scaled(ui->headLb->size());
    ui->headLb->setPixmap(m_mp);

    //b1 识别人脸图像
    cv::Mat faceImage;
    std::vector<uchar> decodeData;
    decodeData.resize(data.size());
    memcpy(decodeData.data(),data.data(),data.size());
    faceImage=cv::imdecode(decodeData,cv::IMREAD_COLOR);

    //int face_id=face_object.face_query(faceImage);//在gui线程中执行资源消耗很大 。GUI线程负责处理用户界面相关的事件,而其他辅助线程则用于执行一些耗时的操作或并行计算等任务。
    //所以将耗时的操作放在辅助线程中执行,而GUI线程可以保持响应,从而提高整体应用程序的响应性和流畅性。同时,也避免了直接在辅助线程中访问GUI对象,从而预防了潜在的线程安全问题
    emit query(faceImage);




}
void face_server::recv_faceID(int64_t faceID){
    qDebug()<<"查询到的face id: "<<faceID;
    if(faceID<0){
        QString sdmsg=QString("{\"employeeID\":\"\",\"name\":\"\",\"department\":\"\",\"time\":\"\"}");
        msocket->write(sdmsg.toUtf8());
    }
    //c1从人脸数据库中提去face_id对应的个人信息
    model.setFilter(QString("faceId=%1").arg(faceID));
    model.select();//显示结果
    qDebug()<<"model.rowCount()"<<model.rowCount();
    if(model.rowCount()==1){
        qDebug()<<"query success ready send data to client";
        QSqlRecord record=model.record(0);
        //{employeeID:%1,name:%2,department:%3,time:%4}
        QString sdmsg=QString("{\"employeeID\":\"%1\",\"name\":\"%2\",\"department\":\"%3\",\"time\":\"%4\"}")
                            .arg(record.value("employeeID").toString()).arg(record.value("name").toString()).arg("软件").arg(QDateTime::currentDateTime().toString("yyyy-MM-dd hh::mm::ss"));

        QString insertAtt=QString("insert into attendance(employeeID) values('%1')").arg(record.value("name").toString());
        qDebug()<<"insertAtt "<<insertAtt;
        QSqlQuery query;
        if(query.exec(insertAtt)){//如果数据插入成功
            msocket->write(sdmsg.toUtf8());//发送数据给客户端
        }
        else{
            QString sdmsg=QString("{\"employeeID\":\"\",\"name\":\"\",\"department\":\"\",\"time\":\"\"}");
            msocket->write(sdmsg.toUtf8());//发送数据给客户端
            qDebug()<<query.lastError().text();
        }
    }
}
