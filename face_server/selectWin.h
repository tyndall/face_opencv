#ifndef SELECTWIN_H
#define SELECTWIN_H

#include <QWidget>
#include<QSqlTableModel>
namespace Ui {
class selectWin;
}

class selectWin : public QWidget
{
    Q_OBJECT

public:
    explicit selectWin(QWidget *parent = nullptr);
    ~selectWin();

private slots:

    void on_quertBt_clicked();

private:
    Ui::selectWin *ui;
    QSqlTableModel *model;
};

#endif // SELECTWIN_H
