#ifndef REGISTERWIN_H
#define REGISTERWIN_H

#include <QWidget>
#include<opencv.hpp>
namespace Ui {
class RegisterWin;
}

class RegisterWin : public QWidget
{
    Q_OBJECT

public:
    explicit RegisterWin(QWidget *parent = nullptr);
    ~RegisterWin();
    //用于实时捕获相机内容
     void timerEvent(QTimerEvent *e);
private slots:
    void on_resetBt_clicked();

    void on_addheadBt_clicked();

    void on_registerBt_clicked();

    void on_openvideoBt_clicked();

    void on_cameraBt_clicked();

private:
    Ui::RegisterWin *ui;

    //用于捕捉开启相机 timerid用于关闭指定的计时器
    cv::VideoCapture cap;
    int timerID;
    cv::Mat srcImage;//1 用于接收摄像头数据(opencv格式，摄像头也是opencv实例化的)
};

#endif // REGISTER_H
