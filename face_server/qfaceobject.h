#ifndef QFACEOBJECT_H
#define QFACEOBJECT_H
#include<seeta/FaceEngine.h>

#include <QObject>
#include<opencv.hpp>
class QFaceObject : public QObject
{
    Q_OBJECT
public:
    explicit QFaceObject(QObject *parent = nullptr);
      ~QFaceObject();
    //人脸数据存储 检测 识别
public slots:
    int64_t face_register(cv::Mat&);
    int64_t face_query(cv::Mat&);
signals:
    void send_faceID(int64_t faceID);
private:
    seeta::FaceEngine *fengineptr;
};

#endif // QFACEOBJECT_H
