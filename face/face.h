#ifndef FACE_H
#define FACE_H

#include <QMainWindow>
#include<opencv.hpp>
#include<QtNetwork>
using namespace cv;
using namespace std;
QT_BEGIN_NAMESPACE
namespace Ui { class Face; }
QT_END_NAMESPACE

class Face : public QMainWindow
{
    Q_OBJECT

public:
    Face(QWidget *parent = nullptr);
    ~Face();
    //1、定时器事件
    void timerEvent(QTimerEvent *e);

private:
    Ui::Face *ui;
    //2、摄像头
    VideoCapture cap;
    //3、CascadeClassifier是OpenCV中一个用于检测特定对象(如人脸、人眼、车辆等)的级联分类器类。它基于Haar特征和AdaBoost算法,使用机器学习方法训练而成。
    //a、加载预训练的分类器文件 b、读取输入图像 c、用detectMultiScale函数在图像上滑动窗口进行对象检测 d、对检测到的对象用矩形框标记输出
    CascadeClassifier cascade;

    //4.计时器和tcp套接字用于控制服务器连接
    QTcpSocket msocket;
    QTimer mtimer;

    int detectNumber;//检测到人脸

    cv::Mat face_Dect;//用于保存识别到的矩形框中的图像

private slots:
    //4.2对应网络的链接状态
    void timer_connect();
    void start_connect();
    void stop_conncet();

    void Recv_data();//接受服务器返回的识别数据




};
#endif // FACE_H
