#ifndef FACE_H
#define FACE_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class Face; }
QT_END_NAMESPACE

class Face : public QMainWindow
{
    Q_OBJECT

public:
    Face(QWidget *parent = nullptr);
    ~Face();

private:
    Ui::Face *ui;
};
#endif // FACE_H
