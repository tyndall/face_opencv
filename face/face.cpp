#include "face.h"
#include "ui_face.h"
#include<QImage>
#include<QDebug>
#include<QJsonDocument>
#include<QJsonObject>
#include<QJsonParseError>
Face::Face(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Face)
{
    ui->setupUi(this);
    //a1 打开摄像头
    if(cap.open(0)){//打开第0个摄像头
        qDebug()<<"open success";
    }//linux平台
    else{
        qDebug()<<"open failed";
    }
    //a2 启动定时器事件
    startTimer(100);
    //a3 导入级联分类器（脸部）
    cascade.load("/opt/opencv4-pc/share/opencv4/haarcascades/haarcascade_frontalface_alt2.xml");

    //b1 开始链接服务器
    connect(&mtimer,&QTimer::timeout,this,&Face::timer_connect);
    mtimer.start(5000);
    //b2 服务器断开链接了
    connect(&msocket,&QTcpSocket::disconnected,this,&Face::start_connect);
    //b3 服务器链接诶成功了
    connect(&msocket,&QTcpSocket::connected,this,&Face::stop_conncet);
    detectNumber=0;//初始检测人数
    //接受服务器发来的数据
    connect(&msocket,&QTcpSocket::readyRead,this,&Face::Recv_data);
    ui->wg_success->hide();


}

Face::~Face()
{
    delete ui;
}
//a2
void Face::timerEvent(QTimerEvent *e)
{
    //a2.1 采集数据
    Mat srcImage;
    if(cap.grab()){
        cap.read(srcImage);                     //a2.2 读取1帧的数据
    }
    else{
        //qDebug()<<"没读到";
    }
    cv::resize(srcImage,srcImage,Size(480,480));//把摄像头采集到的图像设置为显示界面大小

    if(srcImage.data==nullptr){                 //判断cap捕获的1帧图片是否读入到srcImage中

        return ;
    }
    Mat grayImage;
    cvtColor(srcImage,grayImage,COLOR_BGR2GRAY);//a2.2.1转为灰度图可提高检测效率

    std::vector<Rect> faceRects;                //a2.3存储识别到的人脸矩形框架
    cascade.detectMultiScale(grayImage,faceRects);//a2.4 检测人脸 并设置对应的矩形框存在faceRects数组中  函数在该灰度图像上执行人脸检测。检测到的人脸位置矩形存储在 faceRects 变量中。
    if(faceRects.size()>0&&detectNumber>=0){
            qDebug()<<"faceRects success";

        Rect rect=faceRects.at(0);              //a2.5拿到检测到人脸的第一个矩形框
        //rectangle(srcImage,rect,Scalar(0,0,255));//在检测到的那一帧的图像上绘制矩形框
        ui->headpicLb->move(rect.x,rect.y);     //a2.6将图片框png对焦到人脸

        //c1 将考勤客户端捕捉到的数据发送给服务器，不能以图片的形式发送，需要使用QByteArry,使用imencode编码数据
        //c2 把Mat数据转化为QbyteArry,编码成jpg格式
        if(detectNumber>2){//只有检测次数大于2次才会传输数据 避免瞬时检测误传数据

            std::vector <uchar> buf;
            cv::imencode(".jpg",srcImage,buf);
            //c3 要发送的内容 data和size
            QByteArray byteData((const char*)buf.data(),buf.size());
            qint64 backSize=byteData.size();

            //c4 创建用于发送数据的 QByteArray 对象
            QByteArray sendData;
            QDataStream bstream(&sendData,QIODevice::WriteOnly);
            bstream<<backSize<<byteData;
            if(msocket.write(sendData)){
                qDebug()<<"write msocket success";
            }
            else {
                    qDebug()<<"write msocket false";
            }
            detectNumber=-2;
            face_Dect=srcImage(rect);
            cv::imwrite("./cache.jpg",face_Dect);//保存捕获到的人脸图像，用于后续显示头像
        }
        detectNumber++;
    }
    if(faceRects.size()==0){
        ui->headpicLb->move(100,60);//保持图片框在固定位置
        detectNumber=0;
    }

    //把opencv里的bgr5赚到qimage要用的rgb格式
    cvtColor(srcImage,srcImage,COLOR_BGR2RGB);
    QImage image(srcImage.data,srcImage.cols,srcImage.rows,srcImage.step1(),QImage::Format_RGB888);
    QPixmap mmp=QPixmap::fromImage(image);
    mmp.scaledToWidth(ui->videoLb->width());//用于显示的video图像居中
    ui->videoLb->setPixmap(mmp);



}
void Face::Recv_data(){
    QByteArray msg=msocket.readAll();
    qDebug()<<msg;
    QJsonParseError err;
    QJsonDocument doc=QJsonDocument::fromJson(msg,&err);
    if(err.error!=QJsonParseError::NoError){
        qDebug()<<"json格式错误";
        return;
    }
    QJsonObject obj= doc.object();
    QString employeeID=obj.value("employeeID").toString();
    qDebug()<<"employeeID: "<<employeeID;
    QString name=obj.value("name").toString();
    qDebug()<<"name:"<<name;
    QString dateTime=obj.value("time").toString();
    QString department=obj.value("department").toString();

    ui->numberEd->setText(employeeID);
    ui->nameEd->setText(name);
    ui->timeEd->setText(dateTime);
    ui->departmentEd->setText(department);
    ui->headLb->setStyleSheet("border-radius:70px;border-image:url(./cache.jpg);");
    //通过判断传回的ID是否正确决定要不要显示认证成功，同时也可以在错误id返回时隐藏上一次的认证成功
    if(employeeID!="-1"){
        ui->wg_success->show();
    }
    else {
        ui->wg_success->hide();
    }

}
//b1 开始链接服务器
void Face::timer_connect(){
    msocket.connectToHost("10.10.4.152",7776);
    qDebug()<<"服务器正在链接";
}
//b2 服务器断开链接了 尝试通过调用mtimer定时器超时来再次启动链接服务器的slot
void Face::start_connect(){
    mtimer.start(5000);
    qDebug()<<"服务器链接断开";
}
//b3 服务器链接成功了  关闭定时器(不然还会每隔五秒触发一次链接)
void Face::stop_conncet(){
    mtimer.stop();
    qDebug()<<"服务器链接成功";
}
//detectMultiScale(InputArray image, CV_OUT std::vector<Rect>& objects,double scaleFactor = 1.1, int minNeighbors = 3, int flags = 0,Size minSize = Size(),Size maxSize = Size())

//image(InputArray类型): 输入图像,可以是8位无符号整型(uchar)或32位浮点型(float)。objects(std::vector<Rect>类型): 输出参数,用于存储检测到的目标对象的矩形框。
//scaleFactor(double类型): 指定缩放比例因子,用于构建图像金字塔。默认值为1.1,表示每个金字塔层级与上层相比缩小10%。minNeighbors(int类型): 指定每个候选矩形框至少要被检测到多少次,以搜集足够的"邻居",从而确定为有效的对象。默认值为3。较大的值可以增加精确度,降低误报率,但可能造成一些对象遗漏。
//flags(int类型): 设置一些影响检测操作的参数标志位,目前唯一可用的标志是CV_HAAR_SCALE_IMAGE。minSize(Size类型): 设置对象最小尺寸,较小的对象将被忽略。maxSize(Size类型): 设置对象最大尺寸,较大的对象将被忽略。
